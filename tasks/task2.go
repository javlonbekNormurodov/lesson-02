package main

import (
	"fmt"
	"time"
)

func main() {
	dobStr := "01.12.2021" // Replace this date with your birthday
	givenDate, err := time.Parse("02.01.2006", dobStr)
	if err != nil {
		panic(err)
	}
	fmt.Printf("%s is %s\n", givenDate.Format("02-01-2006"), FindWeekday(givenDate))
}

func FindWeekday(date time.Time) (weekday string) {
	// WRITE YOUR CODE HERE
  // Using time.Now().Weekday() function
  var day = date.Weekday()
  var a int = int(day)
	// if(dt == 6 || dt == 7){
	// 	weekday = "weekday"
	// }else{
	// 	weekday = "not weekday"
	// }
	switch a {
	case 1: weekday ="Dushanba"
	case 2: weekday ="Seshanba"
	case 3: weekday ="Chorshanba"
	case 4: weekday ="Payshanba"
	case 5: weekday ="Juma"
	case 6: weekday ="Shanba"
	case 7: weekday ="Yakshanba"
	}
	return 
}


