package main

import (
	"math"
	"fmt"
)

func main() {
	fmt.Println(MySquareRoot(19, 12))
}

func MySquareRoot(num, precision float64) (result float64) {
	// DO NOT USE math.Sqrt!

	// WRITE YOUR CODE HERE
	var degree float64 = 0.5
	num = float64(num)
	precision = math.Pow(num,degree)
	return precision
}
